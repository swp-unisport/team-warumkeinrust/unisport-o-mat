# First steps to UniSport-O-Mat

## Some notes

To ensure a clean development and production environment, we will use `virtualenv`. If you have not installed or used it yet, please refer to the [official documentation](https://pypi.org/project/virtualenv/). 

## Starting `virtualenv`

First create a new folder, in which you want to give this project a new home. Then, create a new `virtualenv` via this command in your terminal

```
virtualenv -p python3 .
```

You'll see a few new folders and a config file for your `virtualenv`. You can start the new environment via this command in your terminal, depending on your OS. 

Linux/MacOS:

```
source bin/activate
```

Windows

```
Scripts/activate
```

If done correctly, you'll see the name of the virtual environment printed at the beginning of your command promt, eg. in bash:

```
(unisport-o-mat) 18:57:55 [foo@bar unisport-o-mat]$
```

## Cloning the project

To clone the project via `git`, please enter one of these commands via your terminal, depending on your preferred way to authenticate.
Via HTTPS:

```
git clone https://git.imp.fu-berlin.de/swp-unisport/team-warumkeinrust/unisport-o-mat.git
```

Via ssh:

```
git clone git@git.imp.fu-berlin.de:swp-unisport/team-warumkeinrust/unisport-o-mat.git
```

## Installing dependencies

You can verify that no python libraries are installed in your `virtualenv`, if you enter the following command via terminal. It should not return any modules right now.

```
python -m pip freeze
```

All required dependency, such as Django, will be installed via `pip` and a text file, such as `requirements.txt`. First, switch the current directory via `cd` into the cloned folder. Then, install the dependencies all at once via this command in your terminal.

```
python -m pip install -r requirements.txt
```

## Starting the django web server

To start the web server, change the directory via `cd` in the project folder. Here you will find the `manage.py` script, which will be used to start commands depending on the Django framework. Start the web server via this command in your terminal.

```
python manage.py runserver
```

If successful, you can now see the running server in your browser at `http://127.0.0.1:8000`.

## Populate the database with test data
To populate the database with some test data run
```
python manage.py seed_db [-y] [--seed SEED] [--no-superuser]
```
All the existing data from your database will be lost!  
Per default a super user called "admin" will be created for development. 
You will be prompted for a password.   
Run `python manage.py seed_db --help` for more information.

## Use the django admin interface to view and edit data during development
If you started the server as described above, you can access the django admin interface on
[localhost:8000/admin](localhost:8000/admin).   
If you seeded the database you can login with username: "admin" and the password you specified.

## Internationalization and how to use it

At the current time, the backend uses [django-modeltranslation](https://django-modeltranslation.readthedocs.io/en/latest/index.html) to handle translations of Strings in the models Question, CallToMove, KnowledgeSnack. The list of available languages is defined in `settings.py`, and the translatable fields are defined in `translation.py`. Right now, the two languages `de` and `en` are enabled, with `de` translations needing to be filled out.

Django internally keeps track of the active language, which decides on the language strings that `object.text` returns. If you want to force a certain language, `object.text_de` can be used.

`"django.middleware.locale.LocaleMiddleware"` has been added as a Middleware. This allows Django to recognize the locale of the requesting Browser and automatically sets the language to that locale. This means that both the Django Admin Panel and our own API choose the Strings depending on the GET request. It is yet to be determined whether React can change the locale in the requests, but if that were the case, this feature would potentially ease the implementation of Serializers for the User-Frontend.

Instead of relying on the Locale given by the Requests, one can also manually change the active language. This example demonstrates how:

```python
from django.utils.translation import get_language, activate

cur_language = get_language() # Returns current active language

activate("de") # Selects German as active language

print(object.text) # Prints German Translation

activate("en") # Selects English as active language

print(object.text) # Prints English Translation

activate(cur_language) # Resets active language to the one before manual activations

```

This might be particularly useful for entering data from the Admin Frontend into the database, or choosing the language for the User Frontend if the Browser Locale cannot be used.


## Pagination in Views

Every list that needs to be paginated needs a paginator.
Here, the Paginator object is created in the GET call.
The Queryset which is supposed to be paginated needs to be run through the function:

```python
new_queryset = paginator.paginate_queryset(complete_queryset, request)
```

The new_queryset is a List, not a Manager, so it can be directly iterated upon.
After the data has been worked on and run through the Serializer as normal,
instead of returning Result(data), the paginator needs to be used again so it can add its page metadata:

```python
return paginator.get_paginated_response(serializer.data)
```

This function already returns a fully valid response, so it can be directly returned.


## Our Handling of REST Standards

In our usecase of the Admin Frontend, we either explicitly work on objects we chose from a list of given objects, or we create a new object all together. Because of these circumstances, it is not relevant for the PUT request to create new objects entirely, since we know that all objects currently being edited exist. This means that the PUT implementations in `views.py` do not create new objects if called with an unknown primary key. Instead, they are similar to PATCH, but require all fields to be sent along instead of just a portion of them as PATCH does.