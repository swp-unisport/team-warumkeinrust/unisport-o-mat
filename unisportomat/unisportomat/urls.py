"""unisportomat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from quiz import views

router = routers.DefaultRouter()
router.register(r"sport", views.SmallSportListView, "small-sport-list")
router.register(r"question", views.SmallQuestionListView, "small-question-list")
router.register(r"snack", views.SnackListView, "snack")
router.register(r"activity", views.ActivityView, "activity")
router.register(r"greeting-end", views.GreetingEndView, "greeting-end")

urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "api/admin/sport-incomplete/",
        views.IncompleteSportView.as_view(),
        name="incomplete",
    ),
    path("api/admin/sport-archive/", views.SportArchiveView.as_view(), name="archive"),
    path("api/admin/sport-scraper/", views.ScraperView.as_view(), name="scraper"),
    path("api/admin/criteria/", views.CriteriaView.as_view(), name="criteria"),
    path("api/admin/question-order/", views.QuestionOrderView.as_view(), name="order"),
    path("api/admin/", include(router.urls)),
    path("api/user/quiz/", views.QuizView.as_view(), name="quiz"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
