"""
Here, every Model which needs translation fields is registered.
"""


from modeltranslation.translator import register, TranslationOptions
from .models import Question, CallToMove, KnowledgeSnack, GreetingText, EndText


@register(Question)
class QuestionTranslationOptions(TranslationOptions):
    """
    Translations for Question-model. Only the text of the question needs to be translated.
    A German Translation is Required.
    """

    fields = ("text",)
    required_languages = ("de",)
    fallback_values = ("No Translation for this Field",)


@register(CallToMove)
class CallToMoveTranslationOptions(TranslationOptions):
    """
    Translations for CallToMove-model. Only the text of the call to move needs to be translated.
    A German Translation is Required.
    """

    fields = ("text",)
    required_languages = ("de",)
    fallback_values = ("No Translation for this Field",)


@register(KnowledgeSnack)
class KnowledgeSnackTranslationOptions(TranslationOptions):
    """
    Translations for KnowledgeSnack-model. Only the text of the knowledge snack needs to be translated.
    A German Translation is Required.
    """

    fields = ("text",)
    required_languages = ("de",)
    fallback_values = ("No Translation for this Field",)


@register(GreetingText)
class GreetingTextTranslationOptions(TranslationOptions):
    """
    Translation options for GreetingText.
    """

    fields = ("text",)


@register(EndText)
class EndTextTranslationOptions(TranslationOptions):
    """
    Translation options for EndText.
    """

    fields = ("text",)
