"""
Adds the ability paginate content provided via REST API.
Can either be given as an argument in a model
or be given globally via settings.py.
"""

from rest_framework.pagination import PageNumberPagination


class PageNumberWithPageSizePagination(PageNumberPagination):
    """
    This pagination style accepts a single number page number
    in the request query parameters.
    SOURCE: https://github.com/bmihelac/ra-data-django-rest-framework
    """

    page_size_query_param = "page_size"
