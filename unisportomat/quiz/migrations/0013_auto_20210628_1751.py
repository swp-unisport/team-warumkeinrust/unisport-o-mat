# Generated by Django 3.2 on 2021-06-28 17:51

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ("quiz", "0012_merge_20210627_2254"),
    ]

    operations = [
        migrations.CreateModel(
            name="GreetingEndTexts",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "greeting",
                    models.TextField(default="Willkommen zum Uni-Sport-O-Mat!"),
                ),
                (
                    "greeting_de",
                    models.TextField(
                        default="Willkommen zum Uni-Sport-O-Mat!", null=True
                    ),
                ),
                (
                    "greeting_en",
                    models.TextField(
                        default="Willkommen zum Uni-Sport-O-Mat!", null=True
                    ),
                ),
                ("end", models.TextField(default="Wähle deinen Sport!")),
                ("end_de", models.TextField(default="Wähle deinen Sport!", null=True)),
                ("end_en", models.TextField(default="Wähle deinen Sport!", null=True)),
            ],
        ),
        migrations.AlterField(
            model_name="sport",
            name="last_used",
            field=models.DateField(default=django.utils.timezone.localdate),
        ),
    ]
