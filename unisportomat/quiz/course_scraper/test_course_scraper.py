"""
Testing module, yo. Just for the course_scraper.py.
"""
from django.test import TestCase
from course_scraper import scraping  # , fetch_website
from collections import OrderedDict


class ScraperTestCase(TestCase):
    """
    Just a few tests, so pylint isn't getting a fit.
    Because reasons.
    """

    def test_returns_dict(self):
        """
        Testing return type of scraping().
        """
        self.assertIsInstance(scraping(), OrderedDict)

    def test_dict_not_empty(self):
        """
        Testing if dict is not empty.
        """
        self.assertTrue(len(scraping()) > 0)
