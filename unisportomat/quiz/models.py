""" Model definitions for the quiz """
import base64
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone


def validate_rating(value):
    """
    This function acts as a validator for ratings.
    Sadly, it isn't called automatically, so it needs to be used manually.
    """

    if value <= -1:
        value = -1

    if not ((10 >= value >= 1) or value == -1):
        raise ValidationError("%s is not a valid rating!" % value)


class CriterionRating(models.Model):
    """
    This is the relation between Sport and Criterion.
    You can use it to add a rating for a specific criterion to a sport.
    To see it's usage check Sport.rate() and Sport.get_rating()
    """

    rating = models.IntegerField(validators=[validate_rating])
    criterion = models.ForeignKey("Criterion", on_delete=models.CASCADE)
    sport = models.ForeignKey("Sport", on_delete=models.CASCADE)

    def __str__(self):
        return str(self.sport) + " - " + str(self.criterion) + ": " + str(self.rating)


class SportManager(models.Manager):
    """
    Manages Creation of Sport Objects
    Since every Criterion Connection needs to be present in the DB at all times,
    the connections are made at creation of the Sport object. For this, Sport objects need to be
    created through this Manager Class.

    Docs: https://docs.djangoproject.com/en/3.2/ref/models/instances/#creating-objects
    """

    def create_sport(self, **kwargs):
        """
        Creates new sport object and every CriterionRating for it
        """
        sport = self.create(**kwargs)
        sport.currently_active = True
        sport.last_used = timezone.localdate()

        for crit in Criterion.objects.iterator():
            sport.rate(crit, -1)

        return sport


class Sport(models.Model):
    """
    Defines a sport with name, url that leads to the booking page.
    A sport includes ratings for all criterions.
    (e.g. How much it corresponds to the criterion "Martial Arts")
    TODO: last_used may be changed in the future to better work with other functionalities, eg. statistics.
    """

    name = models.TextField()
    url = models.URLField()
    criteria_ratings = models.ManyToManyField("Criterion", through="CriterionRating")

    # The Date Field last_used is set to now-time everytime a sport is activated
    # Either through manual activation or activation through the scraper
    last_used = models.DateField(default=timezone.localdate)

    # Boolean currently_active states whether the sport is in the archive or not
    currently_active = models.BooleanField(default=True)

    objects = SportManager()

    def __str__(self):
        return self.name

    def rate(self, criterion, rating):
        """Defines how much (rating) the sport meets the given criterion"""
        rating_obj, _ = CriterionRating.objects.get_or_create(
            sport=self, criterion=criterion, defaults={"rating": rating}
        )
        validate_rating(rating)
        rating_obj.rating = rating
        rating_obj.save()
        return rating_obj

    def get_rating(self, criterion):
        """Returns how much the sport meets the given criterion"""
        criterion_rating = CriterionRating.objects.get(sport=self, criterion=criterion)
        return criterion_rating.rating

    def is_filled(self):
        """
        Returns a Boolean whether all Criterions are given a valid rating (unequal to -1)
        """

        for crit in self.criteria_ratings.iterator():
            if self.get_rating(crit) == -1:
                return False

        return True

    def reactivate(self):
        """
        Sets currently_active to True and updates the last_used Date
        """
        self.currently_active = True
        self.last_used = timezone.localdate()


class CriterionManager(models.Manager):
    """
    Manages Creation of Criterion Objects
    Since every Sport Object needs to be rated in every Criterion,
    when a new Criterion is created, every Sport object needs to be rated for that Criterion.
    As a default value, -1 is entered so that it can be recognized that no true value is given.

    Docs: https://docs.djangoproject.com/en/3.2/ref/models/instances/#creating-objects
    """

    def create_criterion(self, **kwargs):
        """
        Creates a Criterion Object and Rates every existing Sport with -1
        """
        crit = Criterion(**kwargs)

        # Criterion needs to be saved before it can be connected to a sport
        crit.save()

        for sport in Sport.objects.iterator():
            sport.rate(crit, -1)

        return crit


class Criterion(models.Model):
    """
    Defines a Sport property that is used a a criterion for our quiz.
    (e.g. Individual or Team sport)
    """

    name = models.TextField()

    objects = CriterionManager()

    def __str__(self):
        return self.name

    def get_active_sum(self):
        """
        Get Number of Sports with Rating larger than 1 and the cumulated sum of all ratings
        TODO: Think about Usefulness of Rating Sums with 1 as Min Value
        """

        num_active = 0
        rating_sum = 0

        for rating_obj in CriterionRating.objects.filter(criterion=self):

            if rating_obj.rating != -1:
                rating_sum += rating_obj.rating

            if rating_obj.rating > 1:
                num_active += 1

        return num_active, rating_sum


class CallToMove(models.Model):
    """Defines text and image that are used to show a call to move between questions"""

    text = models.TextField()
    image = models.ImageField(null=True, max_length=200)

    def __str__(self):
        return self.text

    def take_base64_image(self, image_data, image_type):
        """
        Takes a String of a Base64 Image and writes it onto the objects image field
        """
        new_image = convert_base64_to_image(image_data, image_type, "activity_img")

        if new_image is None:
            self.image.delete()  # pylint: disable=no-member
            self.image = None

        self.image = new_image


class KnowledgeSnack(models.Model):
    """Defines text and image that are used to show a KnowledgeSnack between questions"""

    text = models.TextField()
    image = models.ImageField(null=True, max_length=200)

    def __str__(self):
        return self.text

    def take_base64_image(self, image_data, image_type):
        """
        Takes a String of a Base64 Image and writes it onto the objects image field
        """
        new_image = convert_base64_to_image(image_data, image_type, "snack_image")

        if new_image is None:
            self.image.delete()  # pylint: disable=no-member
            self.image = None

        self.image = new_image


def convert_base64_to_image(image_data, image_type, image_name):
    """
    Converts a Base64-String into a Django Image File
    """

    if image_data == "":
        return None

    ext = image_type.split("/")[-1]

    image_name += "." + ext

    if image_data.startswith("data:image"):
        image_data = image_data.split(",")[1]

    image = SimpleUploadedFile(
        content=base64.b64decode(image_data),
        name=image_name,
        content_type=image_type,
    )

    return image


class QuestionManager(models.Manager):
    """
    Manages Deletion of Question Objects and their Criteria
    """

    def delete_question(self, pk):
        """
        Deletes a Question and the Criterion connected to it
        """

        question = self.get(pk=pk)
        question.criterion.delete()
        question.delete()


class Question(models.Model):
    """Defines a Question that is assigned to exactly one Criterion"""

    text = models.TextField()
    criterion = models.OneToOneField(
        Criterion, on_delete=models.CASCADE, primary_key=True
    )

    objects = QuestionManager()

    def __str__(self):
        return self.text


class QuestionOrderEntryManager(models.Manager):
    """
    Manages Handling of QuestionOrderEntry Objects
    Includes create_entry_at_end, which handles Creation of new Entries
    and delete_entry, which deletes one entry and changes the order_id of the following entries
    """

    def create_entry_at_end(self, type_of_slot, question_id=None):
        """
        Creates a new OrderEntry at the end of the current Order (so the object gets given the highest order_id)
        """

        if type_of_slot == "question" and question_id is None:
            raise ValueError(
                "A Question ID must be given if the Type of the Slot is Question"
            )
        elif type_of_slot not in [
            "question",
            "snack",
            "activity",
        ]:
            raise ValueError(
                f'{type_of_slot} not in valid choice list ["question", "snack", "activity"]'
            )

        entry = QuestionOrderEntry()

        entry.type_of_slot = type_of_slot

        # If question_id is None, fill it out as -1, else take the question_id value
        if entry.type_of_slot == "question":
            entry.question_id = question_id

        # If no Entry exists, highest_current_order is 0, otherwise highest order_id
        if self.count() == 0:
            highest_current_order = 0
        else:
            # "latest" returns highest value (normally used for dates, I believe)
            highest_current_order = self.latest("order_id").order_id

        entry.order_id = highest_current_order + 1

        entry.save()

        return entry

    def delete_entry(self, given_order_id):
        """
        Delete an Entry in the QuestionOrderEntry Database and decrement every object that had a larger order_id than the one deleted
        """

        # Delete must be called on an instance, not a queryset
        entry = self.get(order_id=given_order_id)
        entry.delete()

        larger_entries = self.filter(order_id__gte=given_order_id)

        # The Primary Key of an Object cannot be changed, really, instead a new Object is created when the PK changes
        # This means that we need to delete the original Object so that we only have the new pk

        for entry in larger_entries.iterator():
            entry.order_id = entry.order_id - 1
            entry.save()

    def delete_entry_by_question_id(self, given_question_id):
        """
        Delete an Entry in the QuestionOrderEntry DB by the Question_ID
        """

        queryset = self.filter(question_id=given_question_id)

        if queryset.count() == 0:
            # If the Question doesn't exist in the Order, we don't need to do anything
            return

        # If the question is in the Order more than once for some reason, sweat not, we will delete them all
        for entry in queryset.iterator():
            self.delete_entry(entry.order_id)


class QuestionOrderEntry(models.Model):
    """
    Defines the order of the Questions, Snacks, and Activities given in the Quiz
    Default Choice for type_of_slot is "snack" instead of "question",
    because "snack" doesn't need a question_id and is therefore a safer choice
    """

    objects = QuestionOrderEntryManager()

    order_id = models.IntegerField(null=True)
    type_of_slot = models.TextField(
        choices=[
            ("question", "question"),
            ("snack", "snack"),
            ("activity", "activity"),
        ],
        default="snack",
    )
    question_id = models.IntegerField(default=-1)

    def __str__(self):
        return f"Entry {self.order_id}: {self.type_of_slot}"


class SingletonModel(models.Model):
    """
    Singleton Model used for Greeting and End Text, so that only one object exists per Model
    """

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        """
        Function for returning the singleton Object
        """
        obj, _created = cls.objects.get_or_create(pk=1)
        return obj


class GreetingText(SingletonModel):
    """
    Database with only one row (if everything is done right)
    Includes start text as column
    """

    text = models.TextField(default="Willkommen zum Uni-Sport-O-Mat!")

    def delete(self, *args, **kwargs):
        self.text_de = self._meta.get_field("text").get_default()
        self.text_en = self._meta.get_field("text").get_default()

    def __str__(self):
        return f"{self.text}"


class EndText(SingletonModel):
    """
    Database with only one row (if everything is done right)
    Includes end text as column
    """

    text = models.TextField(default="Wähle deinen Sport!")

    def delete(self, *args, **kwargs):
        self.text_de = self._meta.get_field("text").get_default()
        self.text_en = self._meta.get_field("text").get_default()

    def __str__(self):
        return f"{self.text}"
