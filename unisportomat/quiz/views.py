"""
Defines the views for the API
"""

import copy
import random
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.db.models.functions import Lower
from .pagination import PageNumberWithPageSizePagination


from .course_scraper.course_scraper import scraping

from .serializers import (
    SingleQuestionSerializer,
    SmallSportListSerializer,
    SingleSportSerializer,
    CriteriaSerializer,
    IncompleteSportSerializer,
    SmallQuestionListSerializer,
    QuestionOrderSerializer,
    SnackTivityListSerializer,
    SingleSnackTivitySerializer,
    ArchiveSerializer,
    GreetingEndSerializer,
)
from .models import (
    Sport,
    Criterion,
    Question,
    QuestionOrderEntry,
    KnowledgeSnack,
    CallToMove,
    GreetingText,
    EndText,
)


# Dev Notes:
# - If we want to include a View in the Router in urls.py, the View needs to be a Viewset
# - Those are mostly meant for Lists of Objects, so instead of get() and post(), list() and create() are used respectively
# https://stackoverflow.com/questions/30389248/how-can-i-register-a-single-view-not-a-viewset-on-my-router


class SmallSportListView(viewsets.ViewSet):
    """
    View for Sports and List of Sport:
    List returns every Sport with the is_filled Field
    Detail returns single Sports with every Criterium
    """

    authentication_classes = []

    # GET for api/admin/sport/
    def list(self, request):
        """
        GET for api/admin/sport/
        Returns a List of Every Sport with the is_filled Field, stating whether every Criterion is given a Rating or not
        """

        paginator = PageNumberWithPageSizePagination()

        order_by_dict = {
            "id": ("pk", False),
            "-id": ("pk", True),
            "name": ("name", False),
            "-name": ("name", True),
            "url": ("url", False),
            "-url": ("url", True),
            "is_filled": ("pk", False),
            "-is_filled": ("pk", True),
        }

        order_by = ("pk", False)
        if "ordering" in request.query_params.keys():
            order_by = order_by_dict[request.query_params.get("ordering")]

        if order_by[0] in ["pk", "-pk"]:
            # Dont Lower when Ordering by PK
            sports = Sport.objects.filter(currently_active=True).order_by(order_by[0])

        # Maybe care for is_filled ordering in the Future?
        elif order_by[1]:
            sports = Sport.objects.filter(currently_active=True).order_by(
                Lower(order_by[0]).desc()
            )
        else:
            sports = Sport.objects.filter(currently_active=True).order_by(
                Lower(order_by[0])
            )

        sports = paginator.paginate_queryset(sports, request)
        is_filled_tuples = []

        for sport in sports:

            is_filled_tuples.append((sport, sport.is_filled()))

        serializer = SmallSportListSerializer(is_filled_tuples)

        return paginator.get_paginated_response(serializer.data)

    # POST for api/admin/sport/
    def create(self, request):
        """
        POST for api/admin/sport/
        View for Creating a New Sport
        """

        request_data = SingleSportSerializer().to_internal_value(request)

        # A Try at Error Catching
        if request_data is None:
            return HttpResponse(status=400)

        new_sport = Sport.objects.create_sport()

        new_sport.name = request_data["name"]
        new_sport.url = request_data["url"]

        new_sport.save()

        response = SingleSportSerializer(new_sport)

        return Response(response.data)

    # GET for api/admin/sport/<id>/
    def retrieve(self, request, pk=None):
        """
        GET for api/admin/sport/<pk>/
        View for getting a Single Sport, with the pk to the Sport being the argument in the URL
        """

        sport = get_object_or_404(Sport, pk=pk)

        response = SingleSportSerializer(sport)

        return Response(response.data)

    # PUT for api/admin/sport/<id>/
    def update(self, request, pk=None):
        """
        PUT for api/admin/sport/<id>/
        Creates a Sport if it doesn't exist, otherwise overwrites it.
        TODO: Maybe Rework PUT if needed of Admin Frontend
        """

        # Get Data from Serializer
        request_data = SingleSportSerializer().to_internal_value(request)

        if request_data is None:
            # Something is Broke, so Refuse Changing Data
            return HttpResponse(status=400)

        # Get Sport from Data
        sport = Sport.objects.get(pk=pk)

        # Apply New Data to Sport
        sport.name = request_data["name"]
        sport.url = request_data["url"]

        if request_data["currently_active"] is True and sport.currently_active is False:
            # If the Sport is just now being activated, update the Date
            sport.reactivate()
        else:
            sport.currently_active = request_data["currently_active"]

        # Overwrite Criterion Ratings
        for criterion, value in request_data["criteria"]:
            sport.rate(criterion, value)

        sport.save()

        # Re-Serialize changed Sport and Send it back
        response = SingleSportSerializer(sport)

        return Response(response.data)

    # PATCH for api/admin/sport/<id>/
    def partial_update(self, request, pk=None):
        """
        PATCH for api/admin/sport/<id>/
        Fills in the given Values into the Sport specified in the URL
        """

        # Get Data from Serializer
        request_data = SingleSportSerializer().to_internal_value(request)

        if request_data is None:
            # Something is Broke, so Refuse Changing Data
            return HttpResponse(status=400)

        # Get Sport from Data
        sport = Sport.objects.get(pk=pk)

        # Apply New Data to Sport, if it exists
        if "name" in request_data.keys():
            sport.name = request_data["name"]

        if "url" in request_data.keys():
            sport.url = request_data["url"]

        if "currently_active" in request_data.keys():
            if (
                request_data["currently_active"] is True
                and sport.currently_active is False
            ):
                # If the Sport is just now being activated, update the Date
                sport.reactivate()
            else:
                sport.currently_active = request_data["currently_active"]

        # Overwrite Criterion Ratings
        if "criteria" in request_data.keys():
            for criterion, value in request_data["criteria"]:
                sport.rate(criterion, value)

        sport.save()

        # Re-Serialize changed Sport and Send it back
        response = SingleSportSerializer(sport)

        return Response(response.data)

    # DELETE for api/admin/sport/<id>/
    def destroy(self, request, pk=None):
        """
        DELETE for api/admin/sport/<id>/
        Removes Sport Object specified in the URL
        """

        sport = get_object_or_404(Sport, pk=pk)

        sport.delete()

        return Response(status=200, data={"message": "Erfolgreich entfernt!"})


class IncompleteSportView(APIView):
    """
    Returns all Sport Objects with Incomplete Ratings
    """

    authentication_classes = []

    # GET for api/admin/sport/incomplete/
    def get(self, request):
        """
        GET for api/admin/sport/incomplete/
        Returns every incomplete Sport with its incomplete Ratings in a paginated manner
        """

        paginator = PageNumberWithPageSizePagination()
        queryset = Sport.objects.filter(currently_active=True).order_by("name")

        incomplete_sport_list = []

        for sport in queryset:

            if not sport.is_filled():
                incomplete_sport_list.append(sport)

        incomplete_sport_list = paginator.paginate_queryset(
            incomplete_sport_list, request
        )

        response = IncompleteSportSerializer(incomplete_sport_list)

        return paginator.get_paginated_response(response.data)


class CriteriaView(APIView):
    """
    View for the List of Criteria and their Metadata
    """

    authentication_classes = []

    # GET for api/admin/criteria/
    def get(self, request):
        """
        GET for api/admin/criteria/
        Returns every Criterium and the Metadata of
        Number of Sports in which the Rating is >1
        and the cumulated sum of Ratings >1
        TODO: Also Pagination
        """

        paginator = PageNumberWithPageSizePagination()

        queryset = paginator.paginate_queryset(
            Criterion.objects.all().order_by("pk"), request
        )

        data = []
        for crit in queryset:

            active_sports, sum_of_weights = crit.get_active_sum()

            data.append((crit, active_sports, sum_of_weights))

        response = CriteriaSerializer(data)

        return paginator.get_paginated_response(response.data)


class SmallQuestionListView(viewsets.ViewSet):
    """
    Viewset for Handling Question Lists and Questions
    """

    # List GET
    def list(self, request):
        """
        api/admin/question GET
        Returns a Paginated List of all Questions with their ID, German and English Text, and Criterion
        """

        paginator = PageNumberWithPageSizePagination()

        questions = Question.objects.all().order_by("pk")
        questions = paginator.paginate_queryset(questions, request)

        response = SmallQuestionListSerializer(questions)

        return paginator.get_paginated_response(response.data)

    # List POST
    def create(self, request):
        """
        api/admin/question POST
        Takes Values of a new Question and Creates a Question and its corresponding Criterion
        """

        new_question_data = SingleQuestionSerializer().to_internal_value(request.data)

        if not (
            "text_de" in new_question_data.keys()
            and "criterion" in new_question_data.keys()
        ):
            return Response(
                status=400,
                data={
                    "message": "Deutsche Übersetzung und Kriterienname müssen ausgefüllt sein!"
                },
            )

        # Test if the Criterion already exists
        queryset = Criterion.objects.filter(name=new_question_data["criterion"])
        if queryset.count() > 0:
            return Response(
                data={
                    "message": f"Kriterium mit Namen {new_question_data['criterion']} existiert bereits!"
                },
                status=400,
            )

        # Otherwise, all is well and we can create them
        new_question = Question()
        new_question.text_de = new_question_data["text_de"]

        if "text_en" in new_question_data.keys():
            new_question.text_en = new_question_data["text_en"]

        new_criterion = Criterion.objects.create_criterion()
        new_criterion.name = new_question_data["criterion"]

        new_question.criterion = new_criterion

        new_question.save()
        new_criterion.save()

        return Response(SingleQuestionSerializer(new_question).data)

    def retrieve(self, request, pk=None):
        """
        api/admin/question/id GET
        Retrieves the Question object given the PK in the URL
        """

        question = get_object_or_404(Question, pk=pk)

        return Response(SingleQuestionSerializer(question).data)

    def update(self, request, pk=None):
        """
        api/admin/question/id PUT
        Completely updates a Question Object given the PK in the URL
        """

        new_question_data = SingleQuestionSerializer().to_internal_value(request.data)

        question = get_object_or_404(Question, pk=pk)

        if (
            "text_de" in new_question_data.keys()
            and "text_en" in new_question_data.keys()
            and "criterion" in new_question_data.keys()
        ):

            # Check if another Criterion with the wanted name already exists
            queryset = Criterion.objects.filter(
                name=new_question_data["criterion"]
            ).exclude(pk=question.criterion.pk)

            if queryset.count() > 0:
                return Response(
                    data={
                        "message": f"Kriterium mit Namen {new_question_data['criterion']} existiert bereits!"
                    },
                    status=400,
                )

            if new_question_data["text_de"] == "":
                return Response(
                    status=400,
                    data={"message": "Es muss eine deutsche Übersetzung geben!"},
                )

            question.criterion.name = new_question_data["criterion"]

            question.text_de = new_question_data["text_de"]
            question.text_en = new_question_data["text_en"]
        else:
            return Response(status=400, data={"message": "Invalide Daten!"})

        question.save()
        question.criterion.save()

        return Response(SingleQuestionSerializer(question).data)

    def partial_update(self, request, pk=None):
        """
        api/admin/question/id PATCH
        Updates a Question Object with given values given the PK in the URL
        """

        new_question_data = SingleQuestionSerializer().to_internal_value(request.data)

        question = get_object_or_404(Question, pk=pk)

        if "text_de" in new_question_data.keys():
            if new_question_data["text_de"] == "":
                return Response(
                    status=400,
                    data={"message": "Es muss eine deutsche Übersetzung geben!"},
                )

        if "criterion" in new_question_data.keys():

            if new_question_data["criterion"] == "":
                return Response(
                    status=400,
                    data={"message": "Der Kriterienname darf nicht leer sein!"},
                )

            # Check if another Criterion with the wanted name already exists
            queryset = Criterion.objects.filter(
                name=new_question_data["criterion"]
            ).exclude(pk=question.criterion.pk)

            if queryset.count() > 0:
                return Response(
                    data={
                        "message": f"Kriterium mit Namen {new_question_data['criterion']} existiert bereits!"
                    },
                    status=400,
                )

            question.criterion.name = new_question_data["criterion"]

        if "text_de" in new_question_data.keys():
            question.text_de = new_question_data["text_de"]

        if "text_en" in new_question_data.keys():
            question.text_en = new_question_data["text_en"]

        question.save()
        question.criterion.save()

        return Response(SingleQuestionSerializer(question).data)

    def destroy(self, request, pk=None):
        """
        api/admin/question/id DELETE
        Deletes the Question given by the ID in the URL and removes it from the QuestionOrderEntry-DB
        """

        question = get_object_or_404(Question, pk=pk)

        QuestionOrderEntry.objects.delete_entry_by_question_id(question.pk)

        Question.objects.delete_question(question.pk)

        return Response(status=200, data={"message": "Erfolgreich Frage gelöscht!"})


class QuestionOrderView(APIView):
    """
    View for Sending and Receiving Question Order Entries
    """

    def get(self, request):
        """
        api/question/order GET
        GETs a Paginated Response containing all Order Entries sorted by their order_id
        """

        paginator = PageNumberWithPageSizePagination()

        queryset = QuestionOrderEntry.objects.all().order_by("order_id")
        paginated_queryset = paginator.paginate_queryset(queryset, request)

        response = QuestionOrderSerializer(paginated_queryset)

        return paginator.get_paginated_response(response.data)

    def post(self, request):
        """
        api/question/order POST
        Receives a new Order for the Questions and overwrites the QuestionOrderEntry DB with the new order
        """

        data = request.data

        # Sort it by Order_id, just in case
        data = sorted(data, key=lambda x: x["id"])

        # Empty old DB
        QuestionOrderEntry.objects.all().delete()

        # Fill it up with new Stuff
        for question_dict in data:
            QuestionOrderEntry.objects.create_entry_at_end(
                question_dict["type"], question_dict["question_id"]
            )

        # Return the whole thing
        return Response(
            QuestionOrderSerializer(
                QuestionOrderEntry.objects.all().order_by("order_id")
            ).data
        )


class SnackTivityView(viewsets.ViewSet):
    """
    Snacks and Activities are equivalent, so they don't need seperate ViewSet definitions.
    Instead only this is defined and the SnackListView and the ActivityListView inherit everything
    and only change the used_objects.
    """

    used_objects = None

    def list(self, request):
        """
        api/admin/<snacks||activities>/ GET
        Gets a List of the Objects with Both translations and a Bool on whether an Image exists
        """

        paginator = PageNumberWithPageSizePagination()

        objects = self.used_objects.objects.all().order_by("pk")
        objects = paginator.paginate_queryset(objects, request)

        serializer = SnackTivityListSerializer(objects)

        return paginator.get_paginated_response(serializer.data)

    def post(self, request):
        """
        api/admin/<snacks||activities>/ POST
        Creates a new Sport, at least text_de must be given
        """

        data = SingleSnackTivitySerializer().to_internal_value(request.data)

        new_object = self.used_objects()  # pylint: disable=not-callable

        if "text_de" in data.keys():
            new_object.text_de = data["text_de"]
        else:
            return Response(
                status=400,
                data={"message": "Das Feld text_de ist für Erstellung benötigt!"},
            )

        if "text_en" in data.keys():
            new_object.text_en = data["text_en"]

        if "image" in data.keys():
            new_object.take_base64_image(data["image"], data["image_type"])

        new_object.save()

        return Response(
            SingleSnackTivitySerializer().to_representation(new_object, request=request)
        )

    def retrieve(self, request, pk=None):
        """
        api/admin/<snacks||activities>/id GET
        Gets a single Sport
        """

        obj = get_object_or_404(self.used_objects, pk=pk)

        return Response(
            SingleSnackTivitySerializer().to_representation(obj, request=request)
        )

    def update(self, request, pk=None):
        """
        api/admin/<snacks||activities>/id PUT
        Assumes every Data was given and fills them out for given PK
        """

        obj = get_object_or_404(self.used_objects, pk=pk)

        data = SingleSnackTivitySerializer().to_internal_value(request.data)

        obj.text_de = data["text_de"]
        obj.text_en = data["text_en"]

        obj.take_base64_image(data["image"], data["image_type"])

        obj.save()

        return Response(
            SingleSnackTivitySerializer().to_representation(obj, request=request)
        )

    def partial_update(self, request, pk=None):
        """
        api/admin/<snacks||activities>/id PUT
        Fills in Given Data in PK
        """

        obj = get_object_or_404(self.used_objects, pk=pk)

        data = SingleSnackTivitySerializer().to_internal_value(request.data)

        if "text_de" in data.keys():
            obj.text_de = data["text_de"]
        if "text_en" in data.keys():
            obj.text_en = data["text_en"]

        if "image" in data.keys():
            obj.take_base64_image(data["image"], data["image_type"])

        obj.save()

        return Response(
            SingleSnackTivitySerializer().to_representation(obj, request=request)
        )

    def destroy(self, request, pk=None):
        """
        api/admin/<snacks||activities>/id DELETE
        Deletes given PK
        """

        obj = get_object_or_404(self.used_objects, pk=pk)

        # Delete Appended Image first
        obj.image.delete()

        obj.delete()

        return Response(status=200, data={"message": "Erfolgreich Gelöscht!"})


class SnackListView(SnackTivityView):
    """
    View for SnackLists and Snacks, inheriting from SnackTivityView
    """

    used_objects = KnowledgeSnack


class ActivityView(SnackTivityView):
    """
    View for ActivityLists and Activities, inheriting from SnackTivityView
    """

    used_objects = CallToMove


class SportArchiveView(APIView):
    """
    View for the list of all archived sports (so all sports with currently_active = False)
    """

    def get(self, request):
        """
        GET for api/admin/archive/
        """

        paginator = PageNumberWithPageSizePagination()

        archived_sports = paginator.paginate_queryset(
            Sport.objects.filter(currently_active=False).order_by("name"), request
        )

        response = ArchiveSerializer(archived_sports)

        return paginator.get_paginated_response(response.data)


class ScraperView(APIView):
    """
    View for the scraper, including GET and POST
    """

    def get(self, request):  # pylint: disable=too-many-locals
        """
        Scrapes the sports currently on the default website,
        and sends a diff of the current sports and the scraped ones
        """

        # Scrape sports from their website
        scraped_sports = scraping()

        # Iterate through DB Sport entries to see which ones are in the new Sport list etc
        diff_list = []

        id_counter = 1

        empty_diff_dict = {
            "id": -1,
            "kind_of_diff": "",
            "old_sport": {
                "id": None,
                "name": "",
                "url": "",
                "last_used": "",
            },
            "new_sport": {
                "name": "",
                "url": "",
            },
        }

        for old_sport in Sport.objects.filter(currently_active=True):

            diff_dict = copy.deepcopy(empty_diff_dict)

            diff_dict["id"] = id_counter

            diff_dict["old_sport"]["id"] = old_sport.pk
            diff_dict["old_sport"]["name"] = old_sport.name
            diff_dict["old_sport"]["url"] = old_sport.url
            diff_dict["old_sport"]["last_used"] = old_sport.last_used

            if old_sport.name in scraped_sports.keys():
                # A Sport currently active is also found in the new scraped sports
                # -> "same" Sport

                diff_dict["kind_of_diff"] = "same"

                new_sport_url = scraped_sports.pop(old_sport.name)

                diff_dict["new_sport"]["name"] = old_sport.name
                diff_dict["new_sport"]["url"] = new_sport_url

            else:
                # A Sport currently active is _not_ found in the new scraped sports
                # -> "to_be_archived" Sport

                diff_dict["kind_of_diff"] = "to_be_archived"

            diff_list.append(diff_dict)
            id_counter += 1

        for new_sport_name, new_sport_url in scraped_sports.items():

            # The query should only contain zero values, or one value.
            # The name is technically not primary key candidate, but they shouldn't be included more than once
            query = Sport.objects.filter(name=new_sport_name)

            diff_dict = copy.deepcopy(empty_diff_dict)

            diff_dict["id"] = id_counter

            diff_dict["new_sport"]["name"] = new_sport_name
            diff_dict["new_sport"]["url"] = new_sport_url

            if query.count() == 0:
                # The new Sport is not found in the Archive, so it is completely new
                # -> "new" Sport

                diff_dict["kind_of_diff"] = "new"

            else:
                # The new Sport is in the Archive, so it needs to be resurrected
                # -> "from_archive" Sport

                old_sport = query.get(name=new_sport_name)

                diff_dict["kind_of_diff"] = "from_archive"

                diff_dict["old_sport"]["id"] = old_sport.pk
                diff_dict["old_sport"]["name"] = old_sport.name
                diff_dict["old_sport"]["url"] = old_sport.url
                diff_dict["old_sport"]["last_used"] = old_sport.last_used

            diff_list.append(diff_dict)
            id_counter += 1

        # Paginate it all!

        paginator = PageNumberWithPageSizePagination()
        paginated_list = paginator.paginate_queryset(diff_list, request)

        return paginator.get_paginated_response(paginated_list)

    def post(self, request):
        """
        Gets list of diffs from Frontend and writes them into the database
        """

        diff_data = request.data

        for diff in diff_data:

            if diff["kind_of_diff"] == "new":

                sport = Sport.objects.create_sport()
                sport.name = diff["new_sport"]["name"]
                sport.url = diff["new_sport"]["url"]

            elif diff["kind_of_diff"] == "same":
                # In case of URL changes, "same" updates the URL

                sport = Sport.objects.get(pk=diff["old_sport"]["id"])

                sport.url = diff["new_sport"]["url"]

                # To update last_used
                sport.reactivate()

            elif diff["kind_of_diff"] == "to_be_archived":

                sport = Sport.objects.get(pk=diff["old_sport"]["id"])
                sport.currently_active = False

            else:
                # -> from_archive

                sport = Sport.objects.get(pk=diff["old_sport"]["id"])
                sport.reactivate()

                # Same as in "same", URL could be different
                sport.url = diff["new_sport"]["url"]

            sport.save()

        return Response(status=200)


class GreetingEndView(viewsets.ViewSet):
    """
    View for handling the beginning sentence
    Method POST isn't allowed, since at all times, a single model for each text exists.
    Method DELETE resets the values to their set default.
    """

    def list(self, request):
        """
        Gives a List with two rows: Greeting in slot 1, End in slot 2.
        Still needs to be paginated thanks to the Admin Frontend Design
        """
        paginator = PageNumberWithPageSizePagination()
        greeting = GreetingText.load()
        end = EndText.load()

        # Needs to be called once so paginator can get meta info
        paginator.paginate_queryset([greeting, end], request)

        data = [
            GreetingEndSerializer(greeting).data,
            GreetingEndSerializer(end).data,
        ]

        return paginator.get_paginated_response(data=data)

    def retrieve(self, request, pk=None):
        """
        Retrieves the Greeting Text if pk==1, else End Text
        """

        if pk == "1":
            obj = GreetingText.load()
        else:
            obj = EndText.load()

        data = GreetingEndSerializer(obj).data

        return Response(data=data)

    def update(self, request, pk=None):
        """
        Updates Greeting or End depending on pk
        """

        data = request.data

        if "text_de" not in data.keys() or "text_en" not in data.keys():
            return Response(
                status=400,
                data={
                    "message": "Es müssen sowohl text_de als auch text_en geliefert werden!"
                },
            )

        if pk == "1":
            obj = GreetingText.load()
        else:
            obj = EndText.load()

        obj.text_de = data["text_de"]
        obj.text_en = data["text_en"]

        obj.save()

        data = GreetingEndSerializer(obj).data

        return Response(data)

    def partial_update(self, request, pk=None):
        """
        Updates Greeting or End depending on pk
        """

        data = request.data

        if pk == "1":
            obj = GreetingText.load()
        else:
            obj = EndText.load()

        if "text_de" in data.keys():
            obj.text_de = data["text_de"]
        if "text_en" in data.keys():
            obj.text_en = data["text_en"]

        obj.save()

        data = GreetingEndSerializer(obj).data

        return Response(data)

    def destroy(self, request, pk=None):
        """
        Resets Object to their Default Texts
        """

        if pk == "1":
            obj = GreetingText.load()
        else:
            obj = EndText.load()

        obj.delete()
        obj.save()

        return Response(status=200, data={"message": "Daten zurückgesetzt!"})


class QuizView(APIView):
    """
    View for providing Questions for Quiz and calculating top ten sports
    """

    def get(self, request):
        """
        Gets all Questions in their correct order with snacks and Activities inbetween them
        """

        queryset = QuestionOrderEntry.objects.all().order_by("order_id")

        data_dict = {
            "start_de": GreetingText.load().text_de,
            "end_de": EndText.load().text_de,
            "start_en": GreetingText.load().text_en,
            "end_en": EndText.load().text_en,
        }

        order_list = []

        for order_entry in queryset:

            entry_dict = {}

            if order_entry.type_of_slot == "question":

                entry_dict["pk"] = order_entry.question_id

                entry_dict["type"] = order_entry.type_of_slot

                entry_dict["text_de"] = Question.objects.get(
                    pk=order_entry.question_id
                ).text_de

                entry_dict["text_en"] = Question.objects.get(
                    pk=order_entry.question_id
                ).text_en

            elif order_entry.type_of_slot == "snack":

                entry_dict["type"] = order_entry.type_of_slot

                # Get random Snack:
                random_snack = random.choice(list(KnowledgeSnack.objects.all()))

                entry_dict["text_de"] = random_snack.text_de
                entry_dict["text_en"] = random_snack.text_en
                if random_snack.image:
                    entry_dict["url"] = request.build_absolute_uri(
                        random_snack.image.url
                    )
                else:
                    entry_dict["url"] = None

            else:

                entry_dict["type"] = order_entry.type_of_slot

                # Get random Activity:
                random_activity = random.choice(list(CallToMove.objects.all()))

                entry_dict["text_de"] = random_activity.text_de
                entry_dict["text_en"] = random_activity.text_en

                if random_activity.image:
                    entry_dict["url"] = request.build_absolute_uri(
                        random_activity.image.url
                    )
                else:
                    entry_dict["url"] = None

            order_list.append(entry_dict)

        data_dict["order_list"] = order_list

        return Response(status=200, data=data_dict)

    def post(self, request):
        """
        Gets Answers of Quiz and returns top ten Sports
        pk: PK of Question
        answer: Answer between 0 and 3
        relevance: Answer between 0 and 5
        """

        criterion_factors = []

        for response in request.data:
            criterion = get_object_or_404(Question, pk=response["pk"]).criterion
            factor = response["answer"] * response["relevance"]

            criterion_factors.append((criterion, factor))

        rated_sports = []

        for sport in Sport.objects.iterator():

            criteria_sum = 0
            criteria_by_ratings = []

            for criterion, factor in criterion_factors:
                rating = sport.get_rating(criterion)

                if rating != -1:
                    factored_rating = rating * factor
                    criteria_sum += factored_rating

                    criteria_by_ratings.append((criterion.name, factored_rating))

            rated_sports.append((sport, criteria_sum, criteria_by_ratings))

        sorted_sport_list = sorted(rated_sports, key=lambda x: x[1], reverse=True)
        top_ten_sports = sorted_sport_list[:10]

        data_list = []
        for sport, criteria_sum, criteria_list in top_ten_sports:

            sorted_criteria_list = sorted(
                criteria_list, key=lambda x: x[1], reverse=True
            )
            top_criteria = sorted_criteria_list[:3]

            data_list.append(
                {
                    "name": sport.name,
                    "link": sport.url,
                    "rating": criteria_sum,
                    "top_criteria": top_criteria,
                }
            )

        return Response(status=200, data=data_list)
