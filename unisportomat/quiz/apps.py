""" Application Configuration for the quiz app """

from django.apps import AppConfig


class QuizConfig(AppConfig):
    """Application Config for the quiz app"""

    default_auto_field = "django.db.models.BigAutoField"
    name = "quiz"
