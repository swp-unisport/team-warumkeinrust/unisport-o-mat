""" Here is the place to register the Models to be seen in the admin interface """

from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

# Register your models here.
from .models import (
    Sport,
    Question,
    Criterion,
    CriterionRating,
    CallToMove,
    KnowledgeSnack,
)


class QuestionAdmin(TranslationAdmin):
    """Allows for proper formatting of Translations in Question"""


class CallToMoveAdmin(TranslationAdmin):
    """Allows for proper formatting of Translations in CallToMove"""


class KnowledgeSnackAdmin(TranslationAdmin):
    """Allows for proper formatting of Translations in KnowledgeSnack"""


admin.site.register(Sport)
admin.site.register(Criterion)
admin.site.register(CriterionRating)

admin.site.register(Question, QuestionAdmin)
admin.site.register(CallToMove, CallToMoveAdmin)
admin.site.register(KnowledgeSnack, KnowledgeSnackAdmin)
