"""
Seeds the database with test data.
You can call this by python manage.py seed_db
"""

import random

from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.utils.translation import activate
from quiz.models import (
    Sport,
    Criterion,
    Question,
    CallToMove,
    KnowledgeSnack,
    QuestionOrderEntry,
    GreetingText,
    EndText,
)


class Command(BaseCommand):
    """
    Seeds the database with test data.
    You can call this by python manage.py seed_db
    """

    help = "Seeds the database with test data"

    def add_arguments(self, parser):
        """
        Add arguments to command that can be added to the command in command line
        In this case there's only one arg that you can call by
        python manage.py seed_db --yes
        """
        parser.add_argument(
            "-y",
            "--yes",
            action="store_true",
            help="Don't ask to confirm database flushing",
        )

        parser.add_argument(
            "--seed",
            type=int,
            default=42,
            help="Optional seed for random generator. Defaults to 42",
        )
        parser.add_argument(
            "--no-superuser",
            action="store_true",
            help="No super user shall be created",
        )

    def handle(self, *args, **options):  # pylint: disable=too-many-locals
        """Create some objects for all models"""

        # delete all present database entries (necessary because of unique constraints)
        call_command("flush", "--no-input" if options["yes"] else [])

        # Create admin user for using django admin interface during development
        if not options["no_superuser"]:
            self.stdout.write("\nSpecify admin password for development:")
            call_command("createsuperuser", "--username=admin", "--email=''")

        # Seed random generator to make this script deterministic
        random.seed(options["seed"])

        # Create sports
        sports_names = [
            "After Work Fitness",
            "Ballett",
            "Basketball",
            "Beachvolleyball",
            "Bouldern",
        ]
        for name in sports_names:
            Sport.objects.create_sport(name=name).save()

        # Create criteria
        criteria_names = [
            "Einzelsport",
            "Mannschaftssport",
            "Ausdauer",
            "Kraft",
            "Kampfsport",
        ]
        for name in criteria_names:
            Criterion.objects.create_criterion(name=name).save()

        # Create ratings for all sports and criterions
        for sport in Sport.objects.all():
            for criterion in Criterion.objects.all():
                sport.rate(criterion, random.randint(1, 10))

        # Create questions
        questions = [
            (
                "Ich würde am liebsten gemeinsam mit anderen trainieren.",
                "I'd prefer to train with others.",
            ),
            (
                "Teamgeist und Wir-Gefühl sind für mich beim Sport eine große Motivation.",
                "Being in a Team is a big motivation for me.",
            ),
            ("Ich betreibe lieber alleine Sport.", "I prefer to do sport alone"),
            (
                "Ich bin bereit, mir ggf. Material für die Sportart zu kaufen.",
                "I am willing to buy extra materials for sport.",
            ),
            (
                "Ich bevorzuge das Sportangebot draußen in der Natur vor dem Indoor-Angebot.",
                "I prefer sports outside in the nature instead of indoors.",
            ),
        ]

        for number, criterion in enumerate(Criterion.objects.all()):
            activate("de")
            que = Question(text=questions[number][0], criterion=criterion)
            activate("en")
            que.text = questions[number][1]
            que.save()

        # Create Calls to Move
        calls_to_move = [
            (
                "Kreise deine Schultern vor der nächsten Frage 3x nach hinten",
                "Move your shoulders in circles. Three Times. Noooow.",
            ),
            (
                "Stehe auf, beuge dich mit gestrecktem Rücken nach vorne und greife deinen Stuhl.",
                "Stand up, keep your back straight, bow down.",
            ),
            ("Mache vor der nächsten Frage 3 Jumping Jacks", "Do Three Jumping Jacks."),
        ]
        with open("quiz/fixtures/images/test_image.png", "rb") as read_file:
            image = SimpleUploadedFile(
                name="test_image.png",
                content=read_file.read(),
                content_type="image/png",
            )

        for text in calls_to_move:
            activate("de")
            c_t_m = CallToMove(text=text[0], image=image)
            activate("en")
            c_t_m.text = text[1]
            c_t_m.save()

        # Create Knowledge Snacks
        knowledge_snacks = [
            (
                "Dass Treppensteigen fast 5x so viele Kalorien verbrennt, wie das Nutzen des Aufzuges?",
                "That Taking the stairs burns five times as much calories than using the lift?",
            ),
            (
                "Dass das Spielemobil zur Mittagszeit immer auf dem Campus unterwegs ist?",
                "That the Spielemobil is on campus every noon?",
            ),
            (
                "Dass regelmäßige Bewegung Herz-Kreislauf-Erkrankungen vorbeugt?",
                "That proper training prevents heart disease?",
            ),
        ]

        with open("quiz/fixtures/images/logo.png", "rb") as read_file:
            image = SimpleUploadedFile(
                name="logo.png",
                content=read_file.read(),
                content_type="image/png",
            )
        for text in knowledge_snacks:
            activate("de")
            k_s = KnowledgeSnack(text=text[0], image=image)
            activate("en")
            k_s.text = text[1]
            k_s.save()

        greeting = GreetingText()
        greeting.save()

        end = EndText()
        end.save()

        # Create Entries in the QuestionOrder DB

        QuestionOrderEntry.objects.create_entry_at_end("question", 1)
        QuestionOrderEntry.objects.create_entry_at_end("snack")
        QuestionOrderEntry.objects.create_entry_at_end("question", 3)
        QuestionOrderEntry.objects.create_entry_at_end("activity")
        QuestionOrderEntry.objects.create_entry_at_end("question", 2)
        QuestionOrderEntry.objects.create_entry_at_end("question", 4)
